import { Component, NgModule, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { SimpleGlobal } from 'ng2-simple-global';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'hgsapps';
  deviceId:any;
  nohdr = false;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private sg: SimpleGlobal,
  ){
    this.sg.headerShow = false;  
    this.sg.sideNavShow = true;  
    
  }

  openNav() {
    document.getElementById('sidenavToggle').style.width = '300px';
    document.getElementById('sidenavToggle').style.left = '0px';
    document.getElementById('sidenavToggle').style.transform = 'translateX(0px)';
    document.getElementById('overlayId').style.width = '100%';
    document.getElementById('overlayId').style.transform = 'translateX(0px)';
}
getLoginData(){
  this.activatedRoute.queryParams.subscribe(params => {            
    this.deviceId =  params['deviceid'];          
    //console.log(this.deviceId);
    if(this.deviceId){
      this.nohdr =  true;
    }else{
      this.nohdr =  false;
    }
  });
}
OnInit(){
}


}
