import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Observable, Subject, ReplaySubject, from, of, range } from 'rxjs';
import { map, filter, switchMap } from 'rxjs/operators';
import { Routes, RouterModule, ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { HttpService } from '../services/http.service';
import { CryptoService} from '../services/crypto.service';

import { AppComponent } from './app.component';
import { LoginPage } from '../pages/login/login';
import { SplashPage   } from '../pages/splash/splash';
import { SideNavPage } from '../pages/side-nav/side-nav';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { FooterPage } from '../pages/footer/footer';

import { hgsConnectPage } from '../pages/hgs-connect/hgs-connect';
import { OtpPage} from '../pages/otp/otp';

import { SimpleGlobal } from 'ng2-simple-global';
import { AuthGuard } from './../services/auth.guard';

import{ notFoundPage } from './../pages/pagenotfound/notFound.Component';

const appRoutes: Routes = [
  { path: '', component: LoginPage},
  { path: 'splash', component: SplashPage, canActivate: [AuthGuard] },
  { path: 'home', component: DashboardPage, canActivate: [AuthGuard] },
  { path: 'hgs-connect', component: hgsConnectPage, canActivate: [AuthGuard]},
  {path: 'authenticate', component: OtpPage, canActivate: [AuthGuard]},
  { path: '404', component: notFoundPage, canActivate: [AuthGuard]},
  { path: '**', redirectTo: '404', canActivate: [AuthGuard] }
 
];



@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    SplashPage,
    DashboardPage,
    SideNavPage,
    FooterPage,
    hgsConnectPage,
    OtpPage,
    notFoundPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [Title, HttpService, CryptoService, SimpleGlobal, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
