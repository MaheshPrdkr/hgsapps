import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpService} from '../../services/http.service';
import { SimpleGlobal } from 'ng2-simple-global';
import { CryptoService} from '../../services/crypto.service';


@Component({
    selector: 'page-splash',
    templateUrl: './splash.html',
})
export class SplashPage {
    employeeId =  this.cryptoService.AES.Decrypt(localStorage.getItem('HGSemployeeId'));
   
    isChecked = false;
    errMsg:any;
    title='Tems & Conditions';
    
    toggleBool: boolean=true;   
    deviceId = localStorage.getItem('deviceID');  
    employeeIDCurrent:any;


    constructor(
        public router: Router,
        private route: ActivatedRoute,
        private httpService: HttpService,
        private sg: SimpleGlobal,
        private cryptoService: CryptoService,
        
    ) {
      this.sg.headerShow = true;
      this.sg.sideNavShow = true;

    }
    ngOnInit() {
        document.title = 'Splash';
    }
    agreeSelected(event){
        if (event.target.checked) {
          this.toggleBool= false;
          //console.log(this.toggleBool + " Cheked");
        }
        else {
          this.toggleBool= true;
          //console.log(this.toggleBool + " UnCheked");
        }
      }
    
    
      agreeChekOnce(){          

        var data = {
          "employeeId":this.employeeId,
          "deviceID": this.deviceId
        }
        return this.httpService.post('api/Inserttnc', data).then(
          (success)=> {
             //console.log(success);
              if(success.data.succeeded == true){
                //console.log('Thank you'); 
                //this.generateOTP(); 
                this.router.navigate(['/hgs-connect']);   
                
              }else{
                //console.log('Unsucceesful'); 
                this.router.navigate(['/']);   
              }
    
          }).catch(    
            (err)=> {
                console.log('in error catch' +err);
                this.router.navigate(['/']);
            })
        }       
        

}