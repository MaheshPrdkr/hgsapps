import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit, Input, NgModule, Pipe } from '@angular/core';
import { SimpleGlobal } from 'ng2-simple-global';
import { Headers, Http, Response, RequestOptions} from '@angular/http';
import { CryptoService } from '../../services/crypto.service';
import { HttpService} from '../../services/http.service';

@Component({
    selector: 'page-notFoundPage',
    templateUrl: './notFound.html',
})
export class notFoundPage {
    EmployeeId:any;

    constructor(
        public router: Router,
        private sg: SimpleGlobal,
        private cryptoService: CryptoService,
        private httpService: HttpService,

    ) {
      this.sg.headerShow = true;
      this.sg.sideNavShow = true;     
      
    }


    ngOnInit() {          
      this.EmployeeId = this.cryptoService.AES.Decrypt(localStorage.getItem('employeeId'));   
      document.title = '404 Page';
    }   


    logoutHome(){
        //console.log(this.EmployeeId);
        const headers = new Headers({ 'Content-Type': 'application/json'});
        const options = new RequestOptions({ headers: headers });          
        var data = {
            employeeId: this.EmployeeId,                     
        };      
        return this.httpService.post('api/logout', data).then(          
            (success)=> {               
                if(success.message = 'Succeeded'){  
                      localStorage.clear();
                      this.router.navigate(['/']); 
                }else{     
                    this.router.navigate(['/']);
                    //console.log('Connection Error');
                }
            }).catch(
                //used Arrow function here
                (err)=> {
                   console.log(err);
                   this.router.navigate(['/']);
                }
             )
        }



}