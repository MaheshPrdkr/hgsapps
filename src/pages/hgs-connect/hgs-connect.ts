import { Component, NgModule, OnInit, Input } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { Router, ActivatedRoute, Params  } from '@angular/router';
import { HttpService} from '../../services/http.service';
import { SimpleGlobal } from 'ng2-simple-global';
import { CryptoService} from '../../services/crypto.service';

declare var $: any;

@Component({
	selector: 'page-hgs-connect',
	templateUrl: './hgs-connect.html',
})
export class hgsConnectPage {

	TokenId : string;
	EmployeeId : string;
    FirstName : string;
	LastName: string;
	EmailId:string;
	

	constructor(
        private route: ActivatedRoute,
		private httpService: HttpService,
		private router: Router,
		private sg: SimpleGlobal,
		private cryptoService: CryptoService,
      ) {
		this.sg.headerShow = false;
		this.sg.sideNavShow = false;
		
	  }

	ngOnInit() {
		//this.emloyeeId = localStorage.getItem('employeeId');	
		document.title = 'HGS Connect';       

	}

	

	
	
}