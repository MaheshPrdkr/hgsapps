import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit, Input, NgModule, Pipe } from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import { Observable, Subject, ReplaySubject, from, of, range } from 'rxjs';
import { map, filter, switchMap } from 'rxjs/operators';

import { HttpService} from '../../services/http.service';
import { SimpleGlobal } from 'ng2-simple-global';
import { CryptoService} from '../../services/crypto.service';

@Component({
    selector: 'page-otp',
    templateUrl: './otp.html',
})
export class OtpPage {

    isMobileVerify:any; 

    employeeId = this.cryptoService.AES.Decrypt(localStorage.getItem('HGSemployeeId'));  
    empEmailId =  this.cryptoService.AES.Decrypt(localStorage.getItem('HGSemailId'));         
    empMobile = this.cryptoService.AES.Decrypt(localStorage.getItem('HGSmobile'));
    hgsTermCon = this.cryptoService.AES.Decrypt(localStorage.getItem('HgsTNCO'));
   // empMobile = "";
    
    otpMessasgeError:any;
    msgtoenterotp:any;
        
    errMsg = localStorage.otpMessasge; 
    errMsg2:any; 

    emailUpdate: boolean=true;    
    loginOTPform: boolean=true;
    wrongOtp:boolean=false;

    deviceId = localStorage.getItem('deviceID');   

    emailIDcheck=true;

    constructor(
        public router: Router,
        private route: ActivatedRoute,
        private httpService: HttpService,
        private sg: SimpleGlobal,
        private cryptoService: CryptoService,
    ) {
      this.sg.headerShow = true;
      this.sg.sideNavShow = true;

      this.otpMessasgeError = localStorage.otpMessasge;    
      
      this.isMobileVerify = localStorage.getItem('DeviceVerifyAlredy');  
    }

    myform: FormGroup;
    otpNumber: FormControl;   

    ngOnInit() {
            
      document.title = 'OTP Authentication';

        this.deviceverify();            
        this.createFormControls();
        this.createForm();
    }



    deviceverify(){

      let emailFirst = this.empEmailId.substring(0,2);
      let emailLast = this.empEmailId.substring(this.empEmailId.lastIndexOf("@")+0);
      let finalEmailId = emailFirst+"******"+emailLast;       

      let mobileFirst = this.empMobile.substring(0,2);      
      let finalMobile = mobileFirst+"********"; 

     
      if (this.isMobileVerify == '1' ) {

          this.emailUpdate=false;
          this.emailIDcheck=true;
          this.loginOTPform=false;  
          this.errMsg = this.otpMessasgeError; 

          //console.log('isMobileVerify = 1');
         
       } else {

        if((this.empEmailId == '' || this.empEmailId == null) && (this.empMobile == '' || this.empMobile == null)){
          
          //console.log('1');

          this.emailUpdate=false;
          this.emailIDcheck=true;
          this.loginOTPform=false;  
          this.errMsg = this.otpMessasgeError; 
          //this.errMsg ="Your Email ID and Mobile number is not register with GIS Portal, Please update and try login." 

        } else if (this.empEmailId == '' || this.empEmailId == null) {

          this.emailUpdate=true;
          this.emailIDcheck=false;
          this.loginOTPform=true;                 
          this.msgtoenterotp  = 'A OTP (One Time Password) has been sent to your Mobile Number "'+finalMobile+'"'  
          //console.log('2');
          
        } else if(this.empMobile == '' || this.empMobile == null){ 

          this.emailIDcheck=false;
          this.emailIDcheck=false;
          this.loginOTPform=true;       
          
          this.msgtoenterotp  = 'A OTP (One Time Password) has been sent to your email id "'+finalEmailId+'"'  
          //console.log('3'); 
          
        } else {         
          this.emailIDcheck=false;
          this.emailIDcheck=false;
          this.loginOTPform=true;       
          
          this.msgtoenterotp  = 'A OTP (One Time Password) has been sent to your email id "'+finalEmailId+'" and Mobile Number "'+finalMobile+'"'  
          //console.log('4');            
        
          //console.log('Enabled');
         
        }


       }    
    }


    createFormControls() {
      this.otpNumber = new FormControl('', Validators.required);     
    }
    createForm() {
        this.myform = new FormGroup({
            otpNumber: this.otpNumber,         
        });
    }

    gotoLogin(){
      this.router.navigate(['/']);
    }
    
      

    validateOTP(){
     
      if (this.myform.value.otpNumber == "" || this.myform.value.otpNumber == null) {
       // console.log('blank');
        this.wrongOtp=true;
        this.errMsg2 = "OTP number is required"
      }

      if (this.myform.valid) {
        var formdata = this.myform.value;
        this.myform.reset();
        var postData = {
          "employeeId":this.employeeId,
          "deviceID": this.deviceId,
          "otp": formdata.otpNumber
        };   
      
      //console.log(postData)
      return this.httpService.post('api/ValidateOTP', postData).then(
        (success)=> {
           //console.log(success);
            if(success.data.isSucceeded == true){
              //console.log('OTP Generate Successfully'); 

             if (this.hgsTermCon == '1') {
              this.router.navigate(['/hgs-connect']);
             } else {
              this.router.navigate(['/splash']);         
            }                      
              

              this.wrongOtp=false;
            }else{
              //console.log('OTP Generate Failed'); 
              this.router.navigate(['/authenticate']);  
              this.errMsg2 = success.data.message;
              this.wrongOtp=true;
              //console.log(this.errMsg2);
            }
  
        }).catch(    
          (err)=> {
              console.log('in error catch' +err);
              this.router.navigate(['/']);
          });

    }
  }



}