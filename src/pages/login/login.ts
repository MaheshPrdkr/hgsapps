import { Component, OnInit, Input, NgModule, Pipe } from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';

import { Observable, Subject, ReplaySubject, from, of, range } from 'rxjs';
import { map, filter, switchMap } from 'rxjs/operators';

import { HttpService} from '../../services/http.service';
import { CryptoService} from '../../services/crypto.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { SimpleGlobal } from 'ng2-simple-global';


@Component({
  selector: 'page-login',
  templateUrl: './login.html'
})

export class LoginPage implements OnInit{
    data:any;
    errMsg:any;
    deviceId:any;   

    lgin = true;
    constructor(
        private httpService: HttpService,
        private cryptoService: CryptoService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private sg: SimpleGlobal
        
    ) {
        this.sg.headerShow = true;
        this.sg.sideNavShow = true;

        this.getDeviceId();   
        
    }
    myform: FormGroup;
    employeeId: FormControl;
    password: FormControl;
    employeeIDCurrent:any;
    
    ngOnInit() {

        document.title = 'Login';
        // if(localStorage.getItem('aleareadyMeet') !== 'true'){
        //     localStorage.clear();
        // }
        this.createFormControls();
        this.createForm();            
        //console.log(this.sg.headerShow);
    }


    createFormControls() {
        this.employeeId = new FormControl('', Validators.required);
        this.password = new FormControl('', Validators.required,);
    }
    
    createForm() {
        this.myform = new FormGroup({
            employeeId: this.employeeId,
            password: this.password,
        });
    }

    onSubmit() {

        if (this.myform.valid) {
          var data = this.myform.value;
          this.myform.reset();

          if (this.deviceId == null) {
                localStorage.setItem('deviceID', '12345');
            } else {
                localStorage.setItem('deviceID', this.deviceId);
            }
          
          var postData = {
              employeeId: this.cryptoService.AES.Encrypt(data.employeeId),
              password: this.cryptoService.AES.Encrypt(data.password),
              deviceID: localStorage.deviceID
          };         
        
            this.employeeIDCurrent =  postData.employeeId
           //console.log(postData);
          return this.httpService.post('api/login', postData).then(          
            (success)=> {           
               //console.log(success);
                //console.log(employeeid);
                if(success.data.isSucceeded == true){
                    localStorage.setItem('HGStoken', success.data.token);
                    localStorage.setItem('HGSfirstName', success.data.fistname);
                    localStorage.setItem('HGSlastName', success.data.lastName);
                    localStorage.setItem('HGSemailId', success.data.email);   
                    localStorage.setItem('HGSemployeeId', this.employeeIDCurrent);
                    localStorage.setItem('HGSmobile', success.data.MoblieNo);
                    localStorage.setItem('HgsTNCO', success.data.HgsTNC);     
                    //localStorage.setItem('employeeId', this.employeeIDCurrent);     
                    
                    //console.log(localStorage.getItem('employeeId')+'Emp Id');
                   
                        if (success.data.IsOtpVerify == 1) {
                            if (success.data.HgsTNC == 1) {
                                this.router.navigate(['/hgs-connect']);    
                            } else {
                                this.router.navigate(['/splash']);
                            }
                            
                            //console.log('authenticate')
                        } else {   
                            this.generateOTP();                         
                        }                            
                 

                }else{
                    //console.log(success);
                    this.lgin = false;
                    this.errMsg = 'Invalid User Name or Password';
                }
            }).catch(
                //used Arrow function here
                (err)=> {
                   console.log(err);
                   this.router.navigate(['/']);
                }
             )
        }
    }

    getDeviceId(){
        this.activatedRoute.queryParams.subscribe(params => {            
            this.deviceId =  params['deviceid'];          
            //console.log(1);

            if (params['deviceid'] != null) {
                localStorage.setItem('deviceID', this.deviceId);
                //console.log(2);
                //console.log(this.deviceId);
            } else {

                if(localStorage.getItem('deviceID') != null || localStorage.getItem('deviceID') == 'undefined')
                {
                    //console.log(3);
                    this.deviceId = localStorage.getItem('deviceID');
                    localStorage.setItem('deviceID', this.deviceId);
                }
               
            }
            
        });
    }


    generateOTP(){
        if (this.deviceId == null) {
            localStorage.setItem('deviceID', '12345');
        } else {
            localStorage.setItem('deviceID', this.deviceId);
        }
        var data = {
          "employeeId":this.cryptoService.AES.Decrypt(this.employeeIDCurrent),
          "deviceID": localStorage.deviceID
        }
        //console.log(data)
        return this.httpService.post('api/GenerateOTP',data).then(
          (success)=> {
             //console.log(success);
              if(success.data.isSucceeded == true){

                localStorage.setItem('otpMessasge', success.data.message);
                this.router.navigate(['/authenticate']);
                localStorage.setItem('DeviceVerifyAlredy', '0');
                //console.log('');               

              }else{    
                  
                if (success.data.DeviceVerify == '1') {
                    localStorage.setItem('DeviceVerifyAlredy', '1');
                    localStorage.setItem('otpMessasge', success.data.message);
                    this.router.navigate(['/authenticate']); 
                    //console.log('');
                    
                } else {
                    localStorage.setItem('DeviceVerifyAlredy', '0');
                    localStorage.setItem('otpMessasge', success.data.message);                   
                    this.router.navigate(['/authenticate']); 
                    //console.log('');
                }
               
                //console.log('OTP Generate Failed');              
              }
    
          }).catch(    
            (err)=> {
                console.log('in error catch' +err);
                this.router.navigate(['/']);
            });
      }

}