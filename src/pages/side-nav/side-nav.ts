import { Component, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params  } from '@angular/router';
import { HttpService} from '../../services/http.service';
import { CryptoService} from '../../services/crypto.service';

@Component({
    selector: 'side-nav',
    templateUrl: './side-nav.html',
})
export class SideNavPage {
    firstName : string;
    lastName : string;
    emailId: string;
    EmployeeId:any;

    constructor(
        private route: ActivatedRoute,
		private httpService: HttpService,
        private router: Router,
        private cryptoService: CryptoService,
      ) {
       
        
        // this.EmployeeId = localStorage.getItem('HGSemployeeId');
        // this.firstName = localStorage.getItem('HGSfirstName');
        // this.lastName = localStorage.getItem('HGSlastName');
        // this.emailId =  localStorage.getItem('HGSemailId'); 

      }

    ngOnInit() {     
        
        this.EmployeeId = this.cryptoService.AES.Decrypt(localStorage.getItem('HGSemployeeId'));
        this.firstName = this.cryptoService.AES.Decrypt(localStorage.getItem('HGSfirstName'));
        this.lastName = this.cryptoService.AES.Decrypt(localStorage.getItem('HGSlastName'));
        this.emailId =  this.cryptoService.AES.Decrypt(localStorage.getItem('HGSemailId')); 

        // console.log(this.employeeName);
        document.title = 'Side Nav';
    }

    closeNav() {
        document.getElementById('sidenavToggle').style.width = '300px';
        document.getElementById('sidenavToggle').style.left = '0px';
        document.getElementById('sidenavToggle').style.transform = 'translateX(-320px)';
        document.getElementById('overlayId').style.width = '100%';
        document.getElementById('overlayId').style.transform = 'translateX(-100%)';
    }

    logout(){

        document.getElementById('sidenavToggle').style.width = '300px';
        document.getElementById('sidenavToggle').style.left = '0px';
        document.getElementById('sidenavToggle').style.transform = 'translateX(-320px)';
        document.getElementById('overlayId').style.width = '100%';
        document.getElementById('overlayId').style.transform = 'translateX(-100%)';

        var data = {
            employeeId: this.EmployeeId,                     
        };          

        return this.httpService.post('api/logout', data).then(          
          (success)=> {               
              if(success.message = 'Succeeded'){  
                  localStorage.clear();

                    this.router.navigate(['/']); 
              }else{     
                  this.router.navigate(['/']);
                  //console.log('Connection Error');
              }
          }).catch(
              //used Arrow function here
              (err)=> {
                 console.log(err);
                 this.router.navigate(['/']);
              }
           )
      }

}    