import { Component, NgModule, OnInit, Input } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { Router, ActivatedRoute, Params  } from '@angular/router';
import { HttpService} from '../../services/http.service';
import { CryptoService} from '../../services/crypto.service';

declare var $: any;

@Component({
	selector: 'page-dashboard',
	templateUrl: './dashboard.html',
})
export class DashboardPage {

	TokenId : string;
	EmployeeId : string;
    FirstName : string;
	LastName: string;
	EmailId:string;
	EmployeeIdnew:any;
	
	constructor(
        private route: ActivatedRoute,
		private httpService: HttpService,
		private router: Router,
		private cryptoService: CryptoService,
      ) {

		this.TokenId = localStorage.getItem('HGStoken');
		this.EmployeeId = localStorage.getItem('HGSemployeeId');
		this.FirstName = localStorage.getItem('HGSfirstName');
		this.LastName = localStorage.getItem('HGSlastName');
		this.EmailId = localStorage.getItem('HGSemailId');	

		localStorage.qsTokenId = this.TokenId;
		localStorage.qsEmployeeId = this.EmployeeId;
		localStorage.qsFirstName = this.FirstName;
		localStorage.qsLastName = this.LastName;
		localStorage.qsEmailId = this.EmailId;

	  }

	ngOnInit() {
		//this.emloyeeId = localStorage.getItem('employeeId');	
		document.title = 'Dashboard';
		
	}


	

	loginModule(appModule){

		if (appModule == 'hgs-Connect') {
			this.HGSConnectApp();
			//console.log('hgs-Connect');
		} else if(appModule == 'tldc-comapss'){
			this.TLDCApp();
			//console.log('tldc-comapss');			
		} else if(appModule == 'ess-leave'){
			this.JamaicaApp();	
			//console.log('ess-leave');
		} else if(appModule == 'ess-personal'){
			//console.log('Comming Soon..');
			
		} else if(appModule == 'my-perfromance'){
			//console.log('Comming Soon..');
		}		
		else {
			//console.log('Dont do tp');
		}				
		
	}


	/* Jemica */
	JamaicaApp(){			
		localStorage.empidEL = 'J00043';
		localStorage.tokenEL = this.TokenId;
		localStorage.fnameEL = this.FirstName;
		localStorage.lnameEL = this.LastName;
		//window.location.href='http://124.30.44.228/HTML_Apps/hgsapps/essleave/';	
		// window.location.href='https://csr.teamhgs.com/hgsapps/leave-web/';	
		//window.location.href='#';		
	}

	/* HGS Connect */
	HGSConnectApp(){
		localStorage.empidH = this.EmployeeId;
		localStorage.tokenH = this.TokenId;
		localStorage.fnameH = this.FirstName;
		localStorage.lnameH = this.LastName;
		localStorage.emailH = this.EmailId;
		//window.location.href='/hgsconnect/';
		//window.location.href='http://124.30.44.228/HTML_Apps/hgsapps/hgsconnect-web/';				
		// window.location.href='https://csr.teamhgs.com/hgsapps/hgsconnect-web/';		
		//window.location.href='#';
	}

	/* TLDC */
	TLDCApp(){		
		//localStorage.tokenT = this.TokenId;
		//localStorage.empidT = this.EmployeeId;
		//localStorage.fnameT = this.FirstName;
		//localStorage.lnameT = this.LastName;
		//window.location.href='/tldc/';
		//window.location.href='http://124.30.44.228/HTML_Apps/hgsapps/tldc-web/';
		// window.location.href='https://csr.teamhgs.com/hgsapps/tldc-web/';		


		//window.location.href='/tldc-web/?tokenT=this.TokenId&&empidT=this.EmployeeId&&fnameT=this.FirstName&&lnameT=this.LastName';
		window.location.href='https://apps.teamhgs.com/hgsapps/tldc-web/?tokenT='+localStorage.qsTokenId+'&&empidT='+localStorage.qsEmployeeId+'&&fnameT='+localStorage.qsFirstName+'&&lnameT='+localStorage.qsLastName;

		//window.location.href='https://csr.teamhgs.com/hgsapps/tldc-web/?tokenT=localStorage.tokenT&&empidT=this.EmployeeId&&fnameT=this.FirstName&&lnameT=this.LastName';		
	
	}

	
	
}