import { Injectable } from '@angular/core';
/// <reference types="crypto-js" />
import * as CryptoJS from 'crypto-js';

@Injectable()
export class CryptoService {

    AES = {
        Encrypt: function (value) {
            //console.log(value);
            if((value || '') == '') {
                return '';
            }            
            var key = CryptoJS.enc.Utf8.parse('7061737323313233');
            var iv = CryptoJS.enc.Utf8.parse('7061737323313233');
            return CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(value), key,
            {
                keySize: 128 / 8,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            }).toString();
        },

        Decrypt: function (value) {    
            
            //console.log('in' + value);            
            var key = CryptoJS.enc.Utf8.parse('7061737323313233');
            var iv = CryptoJS.enc.Utf8.parse('7061737323313233');                 
            ///var encrypted = value.substring(64);

            return CryptoJS.AES.decrypt(value, key,
            {
                keySize: 128 / 8,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            }).toString(CryptoJS.enc.Utf8);           
        }
    };

}
